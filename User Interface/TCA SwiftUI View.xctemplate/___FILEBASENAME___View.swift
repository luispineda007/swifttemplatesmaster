// ___FILENAME___
// ___PROJECTNAME___
//
// Created by ___FULLUSERNAME___ on ___DATE___.
// ___COPYRIGHT___
//

import SwiftUI
import ComposableArchitecture

struct ___VARIABLE_productName___View: View {
    
    let store: Store<<#___VARIABLE_productName___State#>, <#___VARIABLE_productName___Action#>>
    
    init(store: Store<<#___VARIABLE_productName___State#>, <#___VARIABLE_productName___Action#>>) {
        self.store = store
    }
    
    var body: some View {
        WithViewStore(store) { viewStore in
            EmptyView()
        }
    }
}

struct ___VARIABLE_productName___View_Previews: PreviewProvider {
    static var previews: some View {
        ___VARIABLE_productName___View(
            store: Store(
                initialState: <#___VARIABLE_productName___State()#>,
                reducer: <#___VARIABLE_productName___Reducer#>,
                environment: <#___VARIABLE_productName___Environment()#>
            )
        )
    }
}
