// ___FILENAME___
// ___PROJECTNAME___
//
// Created by ___FULLUSERNAME___ on ___DATE___.
// ___COPYRIGHT___
//

import Foundation
import ComposableArchitecture

//MARK:- State
struct ___VARIABLE_productName___State: Equatable {
    
}

//MARK:- Action
enum ___VARIABLE_productName___Action: Equatable {
    case newAction
}

//MARK:- Environment
struct ___VARIABLE_productName___Environment {
    var mainQueue = DispatchQueue.main.eraseToAnyScheduler()
}

//MARK:- Reducer
let <#Domain#>Reducer = Reducer<___VARIABLE_productName___State, ___VARIABLE_productName___Action, ___VARIABLE_productName___Environment> {
    state, action, environment in
    switch action {
    case .newAction:
        return .none
    }
}
